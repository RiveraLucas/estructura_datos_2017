package main;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import appliances.Appliance;
import appliances.Fridge;
import appliances.Stove;
import appliances.WashingMachine;
import appliances.enums.Color;
import appliances.enums.EfficiencyRating;
import appliances.enums.FreezerType;
import appliances.enums.StoveSupplyType;

/**
 * @author Lucas Rivera
 *	Aplicacion principal
 */
public class Main {
	
	Random seed = new Random();
	Scanner scan = new Scanner(System.in);
	DecimalFormat df = new DecimalFormat("#0.00");
	ArrayList<Appliance> appliances = new ArrayList<Appliance>();

	public static void main(String[] args) {
		Main principal = new Main();
		principal.execute();
	}
	
	/**
	 *  Carga n cantidad de electrodomesticos aleatoriamente, los muestra y luego muestra el total
	 */
	private void execute() {

		try {
			System.out.println("How many appliances do you want to create?");
			loadAppliances(scan.nextInt());
			showAppliances();
			System.out.println("Total Price: $" + df.format(getTotalPrice()));
		} catch (Exception e) {
			System.out.println("Error reading value, it must be a positive integer");
		} 
	}

	/**
	 * Obtiene el total de precios finales para todos los electrodom�sticos en la colecci�n.
	 * @return
	 */
	private double getTotalPrice() {
		double total = 0;
		for (Appliance appliance : appliances) {
			double price = appliance.getOverallPrice();
			total+=price;
		}
		return total;
	}
	
	/**
	 * Recorre la colecci�n mostrando la informaci�n de los electrodom�sticos as� como su precio final.
	 * @return
	 */
	private void showAppliances() {
		for (Appliance appliance : appliances) {
			System.out.println(appliance.toString());
			System.out.println("Price: $" + df.format(appliance.getOverallPrice()) + "\n");
		}
	}
	
	/**
	 * Crea una colecci�n de electrodom�sticos cuyos tipos y valores de atributos se seleccionan aleatoriamente.
	 * @param n El numero de elementos a agregar
	 * @throws Exception si es que n es negativo
	 */
	private void loadAppliances(int n) throws Exception{
		if (n<0) throw new Exception();
		for (int i = 0; i < n; i++) {
			Appliance appliance = getRandomAppliance();
			appliance.setColor(Color.getRandomColor());
			appliance.setPowerConsumption(EfficiencyRating.getRandomEfficiency());			
			appliance.setFloorPrice(randomDouble(Appliance.MIN_FLOOR_PRICE, Appliance.MAX_FLOOR_PRICE));
			appliance.setWeight(randomInt(Appliance.MIN_WEIGHT,Appliance.MAX_WEIGHT));
			appliances.add(appliance);
		}
	}
	
	/**
	 * Obtiene una instancia al azar de Heladera,Cocina, o Lavarropas con los valores cargados aleatoriamente
	 * @return
	 */
	private Appliance getRandomAppliance() {
		Appliance appliance = null;
		switch (seed.nextInt(3)) {
		case 0:
			appliance = new WashingMachine(randomInt(WashingMachine.MIN_LOAD, WashingMachine.MAX_LOAD));
			break;
		case 1:
			appliance = new Fridge(FreezerType.getRandomFreezerType(), 
					randomInt(Fridge.MIN_CAPACITY, Fridge.MAX_CAPACITY));
			break;
		case 2:
			appliance = new Stove(randomInt(Stove.MIN_HEIGHT, Stove.MAX_HEIGHT), 
					randomInt(Stove.MIN_WIDTH, Stove.MAX_WIDTH), 
					randomInt(Stove.MIN_DEPTH, Stove.MAX_DEPTH));
			Stove stove = (Stove) appliance;
			stove.setSupplyType(StoveSupplyType.getRandomSupplyType());
			break;
		}
		return appliance;
	}
	
	/**
	 * Devuelve un numero real dentro del rango especificado por rangeMin(inclusive) y rangeMax(inclusive)
	 * @param rangeMin El n�mero m�nimo
	 * @param rangeMax El n�mero maximo
	 * @return El n�mero real dentro del rango
	 */
	private double randomDouble(double rangeMin, double rangeMax) {
		return rangeMin + (rangeMax - rangeMin) * seed.nextDouble();
	}

	/**
	 * Devuelve un numero entero dentro del rango especificado
	 * @param rangeMin El n�mero m�nimo
	 * @param rangeMax El n�mero m�ximo
	 * @return El numero entero dentro del rango
	 */
	private int randomInt(int rangeMin, int rangeMax) {
		return rangeMin + seed.nextInt(rangeMax - rangeMin);
	}

}
