package appliances.enums;

import java.util.Random;

/**
 * Representa la eficiencia energetica de un electrodomestico
 * @author Lucas Rivera
 *
 */
public enum EfficiencyRating{
	A,B,C,D,E,F;
	
	/**
	 * Devuelve uno de los valores posibles de Eficiencia energetica seleccionados al azar
	 * @return
	 */
	public static EfficiencyRating getRandomEfficiency() {
		Random seed = new Random();
		return EfficiencyRating.values()[seed.nextInt(EfficiencyRating.values().length)];
	}
}
