package appliances.enums;

import java.util.Random;

/**
 * Representa los colores que puede tomar un electrodomestico
 * @author Lucas Rivera
 *
 */
public enum Color {
	WHITE,BLACK,RED,BLUE,GRAY;
	
	/**
	 * Devuelve uno de los valores posibles de Color seleccionado al azar
	 * @return
	 */
	public static Color getRandomColor() {
		Random seed = new Random();
		return Color.values()[seed.nextInt(Color.values().length)];
	}
}
