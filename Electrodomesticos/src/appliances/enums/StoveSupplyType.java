package appliances.enums;

import java.util.Random;

/**
 * Representa el tipo de alimentacion de la Cocina
 * @author Lucas Rivera
 *
 */
public enum StoveSupplyType {
	ELECTRIC,GAS;
	
	/**
	 * Metodo de Clase que devuelve uno de los valores posibles de tipo de alimentacion seleccionado al azar
	 * @return
	 */
	public static StoveSupplyType getRandomSupplyType() {
		Random seed = new Random();
		return StoveSupplyType.values()[seed.nextInt(StoveSupplyType.values().length)];
	}
}
