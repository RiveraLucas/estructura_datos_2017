package appliances.enums;

import java.util.Random;

/**
 * Representa el tipo de refrigerador
 * @author Lucas Rivera
 *
 */
public enum FreezerType {
	FREEZER,FRIDGE;
	
	/**
	 * Metodo de clase que devuelve uno de los valores posibles de tipo de freezer seleccionado al azar
	 * @return
	 */
	public static FreezerType getRandomFreezerType() {
		Random seed = new Random();
		return FreezerType.values()[seed.nextInt(FreezerType.values().length)];
	}
}
