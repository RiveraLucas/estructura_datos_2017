package appliances;

import appliances.enums.FreezerType;

/**
 * <p>Clase que representa una Heladera, es una subclase de Appliance(Electrodomestico)
 * @author Lucas Rivera
 *
 */
public class Fridge extends Appliance {

	public static final int MIN_CAPACITY=10, MAX_CAPACITY=80, DEF_CAPACITY=40;
	private FreezerType freezerType;
	private int capacity;
	
	/**
	 * Constructor por defecto, Crea un objeto tipo heladera y 
	 * establece el Tipo de refrigerador en FREEZER y la capacidad en DEF_CAPACITY
	 */
	public Fridge() {
		super();
		this.setFreezerType(FreezerType.FREEZER);;
		this.setCapacity(DEF_CAPACITY);
	}
	
	/**
	 * Crea un objeto tipo heladera y establece los valores de tipo de 
	 * refrigerador y capacidad pasadas por parametros
	 * @param freezerType
	 * @param capacity
	 */
	public Fridge(FreezerType freezerType, int capacity) {
		super();
		this.setFreezerType(freezerType);;
		this.setCapacity(capacity);
	}
	
	
	/**
	 * Obtiene el tipo de refrigerador
	 * @return
	 */
	public FreezerType getFreezerType() {
		return freezerType;
	}
	
	/**
	 * Establece el tipo de refrigerador
	 * @param freezerType
	 */
	public void setFreezerType(FreezerType freezerType) {
		this.freezerType = freezerType;
	}

	/**
	 * Obtiene la capacidad
	 * @return
	 */
	public int getCapacity() {
		return capacity;
	}

	/**
	 * Establece la capacidad, si el valor es incorrecto establece la capacidad por defecto DEF_CAPACITY
	 * @param capacity
	 */
	public void setCapacity(int capacity) {
		this.capacity = (capacity>0)?capacity:DEF_CAPACITY;
	}

	/**
	 * Obtiene el precio final, si la capacidad es mayor a 50, el
	 * precio final se incrementa en 60 si es Frigorífico y en 100 si es Congelador.
	 */
	@Override
	public double getOverallPrice() {
		double overallPrice= super.getOverallPrice();
		if (this.capacity>50) {
			switch (this.freezerType) {
			case FREEZER:
				overallPrice+=100;
				break;
			case FRIDGE:
				overallPrice+=60;
				break;
			}
		}
		return overallPrice;
	}
	
	@Override
	/**
	 * <p>Devuelve una cadena con los valores actuales del Objeto Fridge
	 */
	public String toString() {
		return "FRIDGE ["+super.toString()+"       [Freezer Type= " + freezerType + 
				", Capacity= " + capacity + "liters ]";
	}

}
