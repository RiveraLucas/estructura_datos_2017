package appliances;

import appliances.enums.StoveSupplyType;

/**
 * <p>Clase que representa una Cocina, es una subclase de Appliance(Electrodomestico)
 * @author Lucas Rivera
 *
 */
public class Stove extends Appliance {
	
	public static final int MIN_HEIGHT=80,MAX_HEIGHT=95,
			MIN_WIDTH=50,MAX_WIDTH=120,
			MIN_DEPTH=60,MAX_DEPTH=70,
			DEF_HEIGHT=85,DEF_WIDTH=65,DEF_DEPTH=65;
	private int height;
	private int width;
	private int depth;
	private StoveSupplyType supplyType;
	
	/**
	 * Constructor por defecto, Crea un objeto tipo cocina y 
	 * establece el alto=85,ancho=65 y profundidad=65
	 */
	public Stove() {
		super();
		this.setHeight(DEF_HEIGHT);
		this.setWidth(DEF_WIDTH);
		this.setDepth(DEF_DEPTH);
		this.setSupplyType(StoveSupplyType.GAS);
	}
	
	/**
	 * Crea un objeto tipo cocina y establece los valores de alto
	 * ancho y profundidad por parametros
	 * @param height alto
	 * @param width ancho	
	 * @param depth profundidad
	 */
	public Stove(int height, int width, int depth) {
		super();
		this.setHeight(height);;
		this.setWidth(width);;
		this.setDepth(depth);
	}

	/**
	 * Obtiene el Alto
	 * @return
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Establece el Alto, Si no esta comprendido entre los valores MIN_HEIGHT
	 *  y MAX_HEIGHT se asigna el valor por  defecto DEF_HEIGHT
	 * @param height
	 */
	public void setHeight(int height) {
		this.height = (height>=MIN_HEIGHT && height<=MAX_HEIGHT)? height:DEF_HEIGHT;
	}

	/**
	 * Obtiene el ancho
	 * @return
	 */
	public int getWidth() {
		return width;
	}


	/**
	 * Establece el Ancho, Si no esta comprendido entre los valores MIN_WIDTH
	 *  y MAX_WIDTH se asigna el valor por  defecto DEF_WIDTH
	 * @param width
	 */
	public void setWidth(int width) {
		this.width = (width>=MIN_WIDTH && width<=MAX_WIDTH)?width:DEF_WIDTH;
	}

	/**
	 * Obtiene la profundidad
	 * @return
	 */
	public int getDepth() {
		return depth;
	}
	
	/**
	 * Establece la profundidad, Si no esta comprendido entre los valores MIN_DEPTH
	 *  y MAX_DEPTH se asigna el valor por  defecto DEF_DEPTH
	 * @param depth
	 */
	public void setDepth(int depth) {
		this.depth = (depth>=MIN_DEPTH && depth<=MAX_DEPTH)?depth:DEF_DEPTH;
	}

	/**
	 * Obtiene el tipo de alimentacion
	 * @return
	 */
	public StoveSupplyType getSupplyType() {
		return supplyType;
	}

	/**
	 * Establece el tipo de alimentacion
	 * @param supplyType
	 */
	public void setSupplyType(StoveSupplyType supplyType) {
		this.supplyType = supplyType;
	}

	
	/**
	 * <p>Obtiene el precio final
	 * <p>El precio final se incrementa de acuerdo al ancho
	 * <li>entre 60 a 79  un 15%
	 * <li>entre 80 a 99  un 20% 
	 * <li>m�s de 100 un 30%
	 */
	@Override
	public double getOverallPrice() {
		double overallPrice= super.getOverallPrice();
		if (this.getWidth()>=60 && this.getWidth()<=79)
			overallPrice+=overallPrice*0.15d;
		if (this.getWidth()>=80 && this.getWidth()<=99)
			overallPrice+=overallPrice*0.20d;
		if (this.getWidth()>=100)
			overallPrice+=overallPrice*0.30d;
		return overallPrice;
	}

	@Override
	/**
	 * <p>Devuelve una cadena con los valores actuales del Objeto Stove
	 */
	public String toString() {
		return "STOVE  ["+super.toString()+"       [height= " + height + 
				"cm, width= " + width + "cm, depth= " + depth + "cm, supplyType= " + supplyType + " ]";
	}

}
