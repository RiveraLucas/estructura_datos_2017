package appliances;

/**
 * <p>Clase que representa un Lavarropas, es una subclase de Appliance(Electrodomestico)
 * @author Lucas Rivera
 *
 */
public class WashingMachine extends Appliance {

	public static final int MIN_LOAD=1,MAX_LOAD=20,DEF_LOAD=10;
	private int maxLoad;
	
	/**
	 * Constructor por defecto, establece la Carga en DEF_LOAD
	 */
	public WashingMachine()  {
		super();
		this.setMaxLoad(DEF_LOAD);
	}

	/**
	 * Constructor parametrizado
	 * @param maxLoad Carga Maxima
	 */
	public WashingMachine(int maxLoad) {
		super();
		this.setMaxLoad(maxLoad);
	}
	
	
	/**
	 * @return Obtiene la Carga maxima
	 */
	public int getMaxLoad() {
		return maxLoad;
	}

	/**
	 * Establece la Carga Maxima, se asigna el valor por defecto DEF_LOAD si se ingresa un numero no valido
	 * @param maxLoad
	 */
	public void setMaxLoad(int maxLoad) {
		this.maxLoad=(maxLoad>0)?maxLoad:DEF_LOAD;
	}
	
	@Override
	/**
	 * <p>Obtiene el precio final
	 * <p>Si la carga maxima es mayor a 10 , el precio final se incrementa en un 
	 * porcentaje equivalente a un tercio de la carga.
	 */
	public double getOverallPrice() {
		double overallPrice = super.getOverallPrice();
		if (this.getMaxLoad()>10) {
			overallPrice+= overallPrice * ((double)this.maxLoad/300d);
		}
		return overallPrice;
	}

	@Override
	/**
	 * <p>Devuelve una cadena con los valores actuales del Objeto WashingMachine
	 */
	public String toString() {
		return "WASHER ["+super.toString()+"       [Max Load= " + maxLoad + "Kg ]";
	}

}
