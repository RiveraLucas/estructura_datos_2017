package appliances;

import java.text.DecimalFormat;

import appliances.enums.Color;
import appliances.enums.EfficiencyRating;

/**
 * <p> Clase que representa un Electrodomestico
 * @author Lucas Rivera
 *
 */
public class Appliance {
	
	public static final double MIN_FLOOR_PRICE=100,MAX_FLOOR_PRICE=1000, DEF_FLOOR_PRICE=0;
	public static final int MIN_WEIGHT=10,MAX_WEIGHT=100,DEF_WEIGHT=5;
	
	private double floorPrice;
	private Color color;
	private EfficiencyRating powerConsumption;
	private int weight;
	
	/**
	 * <p><b>Constructor por defecto</b>
	 * <p>Asigna estos valores:
	 * <li>FloorPrice= DEF_FLOOR_PRICE
	 * <li>Color= White
	 * <li>PowerConsumption= F
	 * <li>Weight= DEF_WEIGHT
	 */
	public Appliance()  {
		this.setFloorPrice(DEF_FLOOR_PRICE);
		this.setColor(Color.WHITE);
		this.setPowerConsumption(EfficiencyRating.F);
		this.setWeight(DEF_WEIGHT);
	}
	
	/**
	 * <p>Constructor parametrizado, crea un electrodoméstico con los valores indicados por parámetro
	 * @param floorPrice Precio Base
	 * @param color Enumerado que indica el color
	 * @param powerConsumption Enumerado que indica la eficiencia energetica
	 * @param weight Peso
	 */
	public Appliance(double floorPrice, Color color, EfficiencyRating powerConsumption, int weight)  {
		super();
		this.setFloorPrice(floorPrice);
		this.setColor(color);
		this.setPowerConsumption(powerConsumption);
		this.setWeight(weight);
	}
	
	/**
	 * Obtiene el precio base
	 * @return numero real con el precio base
	 */
	public double getFloorPrice() {
		return floorPrice;
	}
	
	/**
	 * Establece el precio base, si se ingresa un numero no valido se establece por defecto DEF_FLOOR_PRICE
	 * @param floorPrice
	 */
	public void setFloorPrice(double floorPrice) {
		this.floorPrice = (floorPrice>0)?floorPrice:DEF_FLOOR_PRICE;
	}
	
	/**
	 * Obtiene el color
	 * @return
	 */
	public Color getColor() {
		return color;
	}
	/**
	 * Establece el color
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	/**
	 * Obtiene el consumo energetico 
	 * @return Enumerado que indica el consumo de la A a la F
	 */
	public EfficiencyRating getPowerConsumption() {
		return powerConsumption;
	}
	
	/**
	 * Establece el consumo energetico
	 * @param powerConsumption Enumerado que indica el consumo de la A a la F
	 */
	public void setPowerConsumption(EfficiencyRating powerConsumption) {
		this.powerConsumption = powerConsumption;
	}
	
	/**
	 * Obtiene el peso
	 * @return
	 */
	public int getWeight() {
		return weight;
	}
	
	/**
	 * Establece el peso, si el valor ingresado no es valido establece el valor por defecto DEF_WEIGHT
	 * @param weight
	 */
	public void setWeight(int weight){
		this.weight= (weight>0)?weight:DEF_WEIGHT;
	}
	
	
	/**
	 * <p>Obtiene el Precio total dependiendo de los valores de PowerConsumption y Weight
	 * <p>De acuerdo al consumo energético:
	 * <li>si es A agrega 100
	 * <li>si es B agrega 80
	 * <li>si es C agrega 60
	 * <li>si es D agrega 50
  	 * <li>si es E agrega 30
	 * <li>si es F agrega 10
	 * <p>De acuerdo al peso:
	 * <li>entre 0 y 19 kg agrega 10
	 * <li>entre 20 y 49 kg agrega 50
	 * <li>entre 50 y 79 kg agrega 80
	 * <li>mayor que 80 kg agrega 100
	 * @return precio Final
	 */
	public double getOverallPrice() {
		double overallPrice=this.floorPrice;
		switch (this.powerConsumption) {
		case A:
			overallPrice+=100;
			break;
		case B:
			overallPrice+=80;
			break;
		case C:
			overallPrice+=60;
			break;
		case D:
			overallPrice+=50;
			break;
		case E:
			overallPrice+=30;
			break;
		case F:
			overallPrice+=10;
			break;
		}
		if (this.weight>0 && this.weight<=19)
			overallPrice+=10;
		if (this.weight>=20 && this.weight<=49)
			overallPrice+=50;
		if (this.weight>=50 && this.weight<=79)
			overallPrice+=80;
		if (this.weight>=80)
			overallPrice+=100;
		return overallPrice;
	}


	@Override
	/**
	 * <p>Devuelve una cadena con los valores actuales del Objeto Appliance
	 */
	public String toString() {
		DecimalFormat decimalFormat = new DecimalFormat("#.00");
		return "Floor Price= $" + decimalFormat.format(floorPrice) + ", Color= " + color + ", Power Consumption= " + powerConsumption
				+ ", Weight= " + weight + "Kg]\n";
	}	
	
}
